# @semantic-release/bump

[**semantic-release**](https://github.com/semantic-release/semantic-release) plugin to version bump the `package.json` file.


| Step               | Description                                                                                                                                   |                                                                     |
|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------|
| `prepare`          | Update the `package.json` version and [create](https://docs.npmjs.com/cli/pack) the npm package tarball.                                      |                                                                     |

## Install

```bash
$ npm install ssh://git@bitbucket.org:limelighthealth/semantic-release-bump.git -D
```

## Usage

The plugin can be configured in the [**semantic-release** configuration file](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration):

```json
{
  "plugins": [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    "@semantic-release/bump",
  ]
}
```

